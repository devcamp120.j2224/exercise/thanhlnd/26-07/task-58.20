package com.devcamp.s50.task5820.restapi.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import com.devcamp.s50.task5820.restapi.model.CDrink;
import com.devcamp.s50.task5820.restapi.repository.IDrinkRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {
    @Autowired
    IDrinkRepository pCustomeRepository ;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks() {
        try {
            List<CDrink> listcustomer = new ArrayList<CDrink>();
            pCustomeRepository.findAll().forEach(listcustomer::add);
            return new ResponseEntity<List<CDrink>>(listcustomer, HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
